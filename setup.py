try:
    from setuptools import setup, find_packages
    from setuptools.command.test import test as TestCommand
except ImportError:
    from ez_setup import use_setuptools
    use_setuptools()
    from setuptools import setup, find_packages
    from setuptools.command.test import test as TestCommand

class PyTest(TestCommand):
    user_options = [('pytest-args=', 'a', "Arguments to pass to pytest")]

    def initialize_options(self):
        TestCommand.initialize_options(self)
        self.pytest_args = ''

    def run_tests(self):
        #import here, cause outside the eggs aren't loaded
        import sys
        import pytest
        errno = pytest.main(self.pytest_args.split(' '))
        sys.exit(errno)

install_requires = [
    'flask'
]

tests_require = [
    'mock',
    'pytest-cov',
    'coverage',
    'pytest'
]

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name="modules",
    version='0.0.5',
    author="Superolo",
    author_email="olovisani@icloud.com",
    description="Modules POC",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/superolo/modules",
    packages=find_packages(),
    test_suite='tests',
    tests_require=tests_require,
    cmdclass={'test': PyTest},
    install_requires=install_requires,
    include_package_data=True,
    zip_safe=False,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
