from flask import Flask

app = Flask(__name__)

@app.route('/hello_module_one')
def index():
    return 'hello from module_one'

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)