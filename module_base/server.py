from flask import Flask

app = Flask(__name__)

@app.route('/hello_module_base')
def index():
    return 'hello from module_base'

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)